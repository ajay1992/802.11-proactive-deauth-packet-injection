# README #

This project takes advantage of flaw in the data link layer of IEEE 802.11 (wifi) protocol. A denial of service attack is made on wireless network by proactively sniffing for stations connected to targeted access point and injecting deauth packet for them.

 * A deauth packet is injected for each station connected to wireless access point.

 * Sniffer thread continuosly monitors for data packet to detect connected stations.

 * Injector thread injects deauth packet for each station connected to targeted access point.

### Compiling ###

$ gcc -pthread proactive_deauth.c -o deauth

### Running ###

$ ./deauth mon0 "WifiSSID"

where mon0 is wireless interface in monitor mode